diag_log format ["# %1 # Initialising...",time];
#include <modules\modules.h>
#ifndef execNow
    #define execNow call compile preprocessfilelinenumbers
#endif
execNow "scripts\GetParams.sqf";
[] spawn { [(getPos player),"Preparing...",400,200,0,1] spawn BIS_fnc_establishingShot; };
setDate [(DateYear),(DateMonth),(DateDay),(TimeHour),(TimeMinute)];
if (isServer) then {
    diag_log format ["# %1 # Initialising Server...",time];
    setViewDistance (VisualServerViewDistance);
    setObjectViewDistance (VisualServerObjViewDistance);
    setShadowDistance (VisualServerShadViewDistance);
    setTerrainGrid 50;
    enableSaving [false,false];
    { _x setVariable ["BIS_noCoreConversations",true]; } forEach allUnits;
};
if (!(isNull player)) then {
    diag_log format ["# %1 # Initialising Player...",time];
    setViewDistance (VisualPlayerViewDistance);
    setObjectViewDistance (VisualPlayerObjViewDistance);
    setShadowDistance (VisualPlayerShadViewDistance);
    setTerrainGrid (VisualGrass);
    enableSaving [false,false];
    player setVariable ["BIS_noCoreConversations", true];
};
if (!isServer && isNull player) then {
    diag_log format ["# %1 # Initialising JIP...",time];
    waitUntil {!isNull player};
    setViewDistance (VisualPlayerViewDistance);
    setObjectViewDistance (VisualPlayerObjViewDistance);
    setShadowDistance (VisualPlayerShadViewDistance);
    setTerrainGrid (VisualGrass);
    enableSaving [false,false];
    player setVariable ["BIS_noCoreConversations", true];
};
#ifdef mod_dynamicWeather
    if ((WeatherDynamicWeather) == 1) then {
        diag_log format ["# %1 # Initialising Dynamic Weather...",time];
        [(WeatherFog / 100),(WeatherOvercast / 100),(WeatherRain / 100),[(WeatherWind),(WeatherWind)],false] execNow "modules\dynamicWeather\DynamicWeather.sqf";
    };
#endif
#ifdef mod_timeSync
    if ((DateTimeSync) == 1) then {
        diag_log format ["# %1 # Initialising Time Sync...",time];
        fun = [1,true,10,1,false] execFSM "modules\timeSync\core_time.fsm";
    };
#endif
#ifdef mod_BTC_revive
    if ((BtcReviveSwitch) == 1) then {
        diag_log format ["# %1 # Initialising BTC Revive...",time];
        execNow "modules\BTC_revive\BTC_revive_init.sqf";
    };
#endif
#ifdef mod_R3F_logistics
    diag_log format ["# %1 # Initialising R3F Logistics...",time];
    execNow "modules\R3F_logistics\init.sqf";
#endif
#ifdef mod_trindisplay
    diag_log format ["# %1 # Initialising Trin Diving Display...",time];
    execNow "modules\trindisplay\init.sqf";
#endif
#ifdef mod_UPSMON
    if ((UpsmonSwitch) == 1) then {
        diag_log format ["# %1 # Initialising UPSMON...",time];
        execNow "modules\UPSMON\Init_UPSMON.sqf";
        execNow "modules\UPSMON\UPSMON\!R\markerAlpha.sqf";
    };
#endif
#ifdef mod_SLP_Spawn
    if ((SlpSpawnSwitch) == 1) then {
        diag_log format ["# %1 # Initialising SLP Spawn...",time];
        execNow "modules\SLP_Spawn\SLP_init.sqf";
    };
#endif
#ifdef mod_vv_loc
    diag_log format ["# %1 # Initialising VV Locations...",time];
    execNow "modules\vv_loc\init.sqf";
#endif
#ifdef mod_tpwcas
    if ((TpwCasSwitch) == 1) then {
        diag_log format ["# %1 # Initialising TPWCAS...",time];
        [3] execNow "modules\tpwcas\init.sqf";
    };
#endif
#ifdef mod_CEP_caching
    if ((CEPswitch) == 1) then {
        diag_log format ["# %1 # Initialising CEP Caching...",time];
        [(CEPdistance),(CEPdelay)] execNow "modules\CEP_caching\init.sqf";
    };
#endif
#ifdef mod_hcam
    if ((HcamSwitch) == 1) then {
        diag_log format ["# %1 # Initialising Helmet Camera Live Feed...",time];
        execNow "modules\hcam\inithcam.sqf";
    };
#endif
#ifdef mod_taw_vd
    if ((TawVDSwitch) == 1) then {
        diag_log format ["# %1 # Initialising View Distance Settings...",time];
        execNow "modules\taw_vd\init.sqf";
    };
#endif
[60,86400] execNow "scripts\RemoveDead.sqf";
execNow "scripts\KillTicker.sqf";
diag_log format ["# %1 # Initialisation complete.",time];