#include <modules\modules.h>
class Params {
    #include <params.h>
};
#ifdef mod_BTC_revive
    #include <modules\BTC_revive\BTC_respawn.h>
#endif
#ifdef mod_gear
    #include <modules\gear\config.h>
#endif
#ifdef mod_R3F_logistics
	#include <modules\R3F_logistics\desc_include.h>
#endif
#ifdef mod_trindisplay
	#include <modules\trindisplay\dialog\define.hpp>
#endif
#ifdef mod_taw_vd
    #include <modules\taw_vd\dialog.hpp>
#endif
class RscTitles {
#ifdef mod_R3F_logistics
	#include <modules\R3F_logistics\desc_rsct_include.h>
#endif
#ifdef mod_trindisplay
	#include <modules\trindisplay\dialog\trin_display.hpp>
#endif
};
class CfgSounds {
	sounds[] = {};
#ifdef mod_trindisplay
	#include <modules\trindisplay\config.h>
#endif
};