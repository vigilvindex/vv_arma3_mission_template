VV Arma 3 Mission Template
==========================
This is a mission template to help with making missions in Arma 3 that use scripts that the community has made. It includes scripts from other authors, this readme file contains attribution information and links.

The intention is that by maintaining this template and it file and folder naming structure, it will help ensure the inter-operability and compatibility of these scripts for reuse when making new missions.

Thanks:

Tonic:
* Virtual Ammobox System - http://forums.bistudio.com/showthread.php?149077-Virtual-Ammobox-System-(VAS)
* MP Fast Time - http://forums.bistudio.com/showthread.php?137061-MP-Fast-time-FSM-JIP-Compatible
* TAW View Distance Settings - http://www.armaholic.com/page.php?id=19752

Giallustio:
* BTC Revive - http://www.giallustio.altervista.org/pages/=BTC=_revive.php

Ollem:
* TPWCAS - http://forums.bistudio.com/showthread.php?143797-TPWCAS-TPWC-AI-Suppression-System/page9

Nomadd:
* SLP Spawn - http://forums.bistudio.com/showthread.php?147873-SLP-Spawning-script

Kronzky & Monsada & Ligthert:
* UPSMON - http://forums.bistudio.com/showthread.php?147808-UPSMON-for-arma3&p=2318001&viewfull=1#post2318001

Tajin:
* Helmet Camera Live Feed Script - http://www.armaholic.com/page.php?id=19614

Celery:
* CLY Remove Dead Script - http://forums.bistudio.com/showthread.php?113434-CLY-Remove-Dead-script

Engima:
* Dynamic Weather Effects - http://forums.bistudio.com/showthread.php?130595-Dynamic-Weather-Effects-Script-Release

Tuliq:
* Basic Kill Ticker - http://forums.bistudio.com/showthread.php?148964-SCRIPT-Basic-killticker

Tophe:
* Simple Vehicle Respawn - http://forums.bistudio.com/showthread.php?76445-Simple-Vehicle-Respawn-Script

Trini Scourge:
* Trin Diver Stats Display - http://forums.bistudio.com/showthread.php?150015-Diver-Stats-A-light-simulation-of-depth-pressure-and-air-consumption-rates

Madbull:
* R3F Logistics - http://forums.bistudio.com/showthread.php?94280-R3F-Artillery-and-Logistic-Manual-artillery-and-advanced-logistic-(mission-script)

Desrat:
* Randon House Garrison Script - http://forums.bistudio.com/showthread.php?150051-Random-House-Garrison-Script-v1-0

=7Cav=CW2.Iher.I:
* Rearm, Refuel & Repair Script - http://www.armaholic.com/page.php?id=19139

Wolffy.au & -eutf-Myke:
* CRB/CEP AI Caching - http://creobellum.org/node/21

Multi-Session Operations (MSO) Dev Team:
* MSO - https://dev-heaven.net/projects/mso

Bohemia Interactive:
* Arma 3 - http://www.bistudio.com/

Armaholic:
* News - http://www.armaholic.com/

Community:
* Forums - http://forums.bistudio.com/
