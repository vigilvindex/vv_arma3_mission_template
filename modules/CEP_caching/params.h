class CEPswitch { 
    title       = "AI - CEP Caching:";
    values[]    = {0,1};
    texts[]     = {"Off","On"};
    default     = 0;
};
class CEPdistance { 
    title       = "AI - CEP Caching - Distance:";
    values[]    = {500,1000,1500,2000,2500,3000,3500,4000};
    texts[]     = {"500m","1km","1.5km","2km","2.5km","3km","3.5km","4km"};
    default     = 1000;
};
class CEPdelay { 
    title       = "AI - CEP Caching - Delay:";
    values[]    = {0,1,5,10,30,60};
    texts[]     = {"None","1 Second","5 Seconds","10 Seconds","30 Seconds","1 Minute"};
    default     = 1;
};
