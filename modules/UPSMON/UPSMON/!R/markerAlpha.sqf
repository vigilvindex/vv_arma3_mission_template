/* =============================================
	!R
	Hide area markers.
	
	create Game Logic Object
	put in initialization field:
	
		nul = call compile preprocessFile "scripts\UPSMON\!R\markerAlpha.sqf";

	all markers area must be named area0, area1...area13
		
================================================= */

{ _x setmarkeralpha 0; } foreach ["respawn_west","respawn_east","town_jay_cove","town_agios_cephas","town_agios_ioannis","camp_maxwell","town_girna","camp_tempest","station_mike","airstrip","kamino_firing_range","town_agia_marina","camp_rogain","old_outpost","kill_farm"]; 