class UpsmonSwitch { 
    title       = "AI - UPSMON:";
    values[]    = {0,1};
    texts[]     = {"Off","On"};
    default     = 1;
};
class UpsmonDebug { 
    title       = "AI - UPSMON - Debug:";
    values[]    = {0,1};
    texts[]     = {"Off","On"};
    default     = 0;
};