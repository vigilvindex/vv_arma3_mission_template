//Diver Stats by triniscourge: For Open Circuit Recreational Diving------
//-----------------------------------------------------------------------
_unit = _this select 0;
_sacRT = _this select 1; 
_tankSize = _this select 2;
_maxppO = _this select 3; //Max ppO2 (atm). Determines the Maximum Operating Depth of the Diver.
_percO2 = _this select 4; //Percentage of Oxygen in breathing mix.
_percN2 = _this select 5; //Percentage of Nitrogen in breathing mix.
_percHe = _this select 6; //Percentage of Helium in breathing mix.

_airTotal = _percO2 +_percN2 +_percHe;
_psi = _tankSize; //Starting PSI of a full tank.
_tisK = (ln 2)/4;

_airConsumption = 0;
_ascTime = 0;
_maxDepth = 0;
_timeleft = 0;
_diveTime = 0;
_diverBearing = 0;
_tisConst = 0;
_totalTis = 0;

//---Narcosis Variables---
_narcFactor = 0;
_narcEffectNeg = 0;
_affectedColor = 0;
_affectedBlur = 0;
_affectedChroma = 0;

//---Deco Variables---
_decoTime = 0;
_depth2deco = 0;
_depth2deepStop = 0;
_deepStopTime = 0;
_diveCnt = 0;
_doDeco = 0;
_deepStopCeil = 0;
_tisAval = 0;
_tisBval = 0;
_AscCeil = 0;
_AscCeilB = 0;
_AscCeilA = 0;
_HeAmt = 0.000001;
_N2Amt = 0.000001;
_O2Amt = 0.000001;


//---Depth Variables---
_depth = 0;
_depthA = 0;
_depthB = 0;
_dDepth = 0;
_maxOD = 0;

//---Pressure Variables---
_pressure = 0;
_pressureA = 0;
_pressureB = 0;
_dPressure = 0;

//---Gas Variables---
//--O2--
_oTot = 0;
_oPerc = 0;
_O2TisTot = 0;
_pOAlv = 0;
//--N2--
_nTot = 0;
_nPerc = 0;
_nTisTot = 0;
_pNAlv = 0;
//--He--
_HeTot = 0;
_HeTisTot = 0; 
_pHeAlv = 0;


//---Temp Variables---
_tempC = 22;
_tempF = 0;
_tempK = 0;

/*
Usage: In diver init insert: eg. null = [this, 25, 3000, 1.1, 0.15, 0.30, 0.55] execVM "scripts\trindisplay\diverstats.sqf" 
The values in the array represent [diver, SAC Rate, TankSize, ppO2 Threshold (advanced divers), %O2 in breathing mix(decimal),%N2 in breathing mix(decimal),%He in breathing mix(decimal)]
It is possible to create any mixture of O2, N2 and He (as long as their sum is 1)
*/

//Loop
scopeName "main";
if (isServer && !isDedicated) then 
{

	private "_pGridPos";

	while {alive _unit && _psi >= 0 && player == _unit} do
 {
	scopeName "uw_loop";
	if (underwater _unit) then
			{				
			
			/* ---DEBUG---
			hint format ["ppN2 %1 \n ppHe %2 \n ppO2 %3 \n Max OD %4 \n Asc Ceiling A %5 \n DecoTime %6 \n Deep Stop: %7 \n DS Time: %8 \n Narc Factor %9 \n %10", (round(_pNAlv *10))/10, (round(_pHeAlv *10))/10, (round(_pOAlv *10))/10,(round(_maxOD *10))/10, _AscCeilA, [((_decoTime)/60)+.01,"HH:MM"] call bis_fnc_timetostring, (round(_deepStopCeil /5) *5),[((_deepStopTime)/60)+.01,"HH:MM"] call bis_fnc_timetostring, _narcFactor, _narcEffectNeg];
			*/
			
			_pGridPos = mapGridPosition _unit;	
			
			//Check if player enters valid percentages for breathing gas
			if (_airTotal >1) then
			{
				_unit sideChat "Error in init argument: Sum of breathing gasses should equal 1";
				breakOut "uw_loop";
			}
				else { 
					if (_airTotal < 1) then
						{
							_unit sideChat "Error in init argument: Sum of breathing gasses should equal 1";
							breakOut "uw_loop";
						}
					};
			
			//Main display elements						
			disableSerialization;
			2 cutRsc ["trin_disp","PLAIN"];
			_displayUI = uiNamespace getVariable 'trin_disp';
			(_displayUI displayCtrl 1111) ctrlSetText format["%1",[((round(_ascTime))/60)+.01,"HH:MM"] call bis_fnc_timetostring];
			(_displayUI displayCtrl 1112) ctrlSetText format["%1",(round(_depth *10))/10];
			(_displayUI displayCtrl 1113) ctrlSetText format["%1",[((_diveTime)/60)+.01,"HH:MM"] call bis_fnc_timetostring];
			(_displayUI displayCtrl 1114) ctrlSetText format["%1",(round(_maxDepth *10))/10];
			(_displayUI displayCtrl 1115) ctrlSetText format["%1",[((_timeleft)/60)+.01,"HH:MM"] call bis_fnc_timetostring];
			(_displayUI displayCtrl 1116) ctrlSetText format["%1",(round(_pressure *10))/10];
			(_displayUI displayCtrl 1117) ctrlSetText format["%1",round(_airConsumption)];
			(_displayUI displayCtrl 1118) ctrlSetText format["%1",round(_psi)];			
			(_displayUI displayCtrl 1121) ctrlSetText format["%1",(round(_dDepth *10))/10];
			(_displayUI displayCtrl 1122) ctrlSetText format["%1",(round(_nPerc *10))/10];
			(_displayUI displayCtrl 1126) ctrlSetText format["%1",round(_diverBearing)];
			(_displayUI displayCtrl 1127) ctrlSetText format["%1 C | %2 K",(round(_tempC *10))/10, (round(_tempF *10))/10];
			(_displayUI displayCtrl 1128) ctrlSetText format["%1",_pGridPos];
			(_displayUI displayCtrl 1129) ctrlSetText format["%1",_AscCeil];
			(_displayUI displayCtrl 1130) ctrlSetText format["%1",[((_decoTime)/60)+.01,"HH:MM"] call bis_fnc_timetostring];
			(_displayUI displayCtrl 1131) ctrlSetText format["%1",(round(_deepStopCeil /5) *5)];
			(_displayUI displayCtrl 1132) ctrlSetText format["%1",[((_deepStopTime)/60)+.01,"HH:MM"] call bis_fnc_timetostring];
			(_displayUI displayCtrl 1133) ctrlSetText format["MAX OP %1FT",round(_maxOD)];
					
			_diveTime = _diveTime + 1;
			_depth = (((getPosASL _unit) select 2)* -3.28); //Conversion to feet (inverted for pressure calculation)
			_pressure = ((_depth / 33) + 1);
			_depth2deco = _depth - _AscCeil;
			_depth2deepStop = _depth - _deepStopCeil;
			_diverBearing = getdir _unit;
			
						
			//---N2/O2/He Gas calculations; See functions\trin_fn_gasCalc.sqf---
			_tisConst = (-_tisK *(_diveTime/60));			
			_tempK = _tempC + 273.15;
			_tempF = (_tempC *1.8) +32;
			_nTot = [_pressure, _percN2, 1600, _tempK] call getGasTot;
			_oTot = [_pressure, _percO2, 756.7, _tempK] call getGasTot;
			_HeTot = [_pressure, _percHe, 2865, _tempK] call getGasTot;
			_totalTis = _nTisTot + _HeTisTot + _O2TisTot;
			_pNAlv = _percN2 *(_pressure - 0.0567);
			_pHeAlv = _percHe *(_pressure - 0.0567);
			_pOAlv = _percO2 *(_pressure - 0.0567);
			_maxOD = ((_maxppO/_percO2) -1) *33;
			
			_nPerc = ((_nTot /1000)/5) *100;
			_oPerc = ((_oTot /1000)/5) *100;
			_HePerc = ((_HeTot /1000)/5) *100;
			
			_narcFactor = _pOAlv +_pNAlv +(0.23 *_pHeAlv);
			_eNdepth = ((_narcFactor -1) *10) *3.28;		
			_narcEffectNeg = 1 -(_narcFactor /10);
			
			_tisAval = ((1.37 *_nTisTot) +(0.0346 *_HeTisTot) +(1.382 *_O2TisTot))/ ((_N2Amt *1.37) + (_HeAmt *0.0346) + (_O2Amt *1.382));
			_tisBval = ((0.03870 *_nTisTot) +(0.02380 *_HeTisTot) +(0.03186 *_O2TisTot))/ ((_N2Amt *0.03870) + (_HeAmt *0.02380) +(0.03186 *_O2Amt));
			_AscCeilA = (((_nTisTot +_HeTisTot +_O2TisTot) -_tisAval) *_tisBval) *3.28;
						
			_airConsumption = (_pressure * _sacRT);
			_psi = (_psi - (_airConsumption /60));
			_timeleft = ((_psi / _airConsumption) *60);
			_depthA = (((getPosASL _unit) select 2)* -3.28);
			_pressureA = ((_depthA / 33) + 1);
			
			//Display elements for ascent indicators			
				switch (true) do
			{
					case (_dDepth > 0): {
						(_displayUI displayCtrl 1119) ctrlSetText "modules\trindisplay\images\upArrow.paa";
						(_displayUI displayCtrl 1120) ctrlSetText "^";
						};
					case (_dDepth < 0): {
						(_displayUI displayCtrl 1119) ctrlSetText "modules\trindisplay\images\downArrow.paa";
						(_displayUI displayCtrl 1120) ctrlSetText "v";
						};
					case (_dDepth == 0): {
						(_displayUI displayCtrl 1119) ctrlSetText "modules\trindisplay\images\noArrow.paa";
						(_displayUI displayCtrl 1120) ctrlSetText "";
						};						 					
			};
			
			//Controls for Ascent Ceiling Calculation.
			if (_percHe > 0) then
			{
				_HeAmt = 1;
			}
				else 
				{
					_HeAmt = 0;
				};
			
			if (_percN2 > 0) then
			{
				_N2Amt = 1;
			}
				else 
				{
					_N2Amt = 0;
				};
			
			//Display element for ascent warning
			if (_dDepth > 8) then			 
				{						
					(_displayUI displayCtrl 1133) ctrlSetText "";
					(_displayUI displayCtrl 1125) ctrlSetText "FAST ASCENT!";
					playSound "trin_dispExit";
				};	
			
			//Display elements for tissue saturation
			switch (true) do
			{
						case (_nTisTot <= 0): {					
						(_displayUI displayCtrl 1124) ctrlSetText "modules\trindisplay\images\tis_1.paa";					
						};	
						case ((_nTisTot > 0) && (_nTisTot <= 0.44)): {					
						(_displayUI displayCtrl 1124) ctrlSetText "modules\trindisplay\images\tis_1.paa";					
						};
						case ((_nTisTot > 0.44) && (_nTisTot <= 0.66)): {					
						(_displayUI displayCtrl 1124) ctrlSetText "modules\trindisplay\images\tis_2.paa";					
						};
						case ((_nTisTot > 0.66) && (_nTisTot <= 0.88)): {					
						(_displayUI displayCtrl 1124) ctrlSetText "modules\trindisplay\images\tis_3.paa";					
						};
						case ((_nTisTot > 0.88) && (_nTisTot <= 1.1)): {					
						(_displayUI displayCtrl 1124) ctrlSetText "modules\trindisplay\images\tis_4.paa";					
						};
						case ((_nTisTot > 1.1) && (_nTisTot <= 1.32)): {					
						(_displayUI displayCtrl 1124) ctrlSetText "modules\trindisplay\images\tis_5.paa";					
						};
						case ((_nTisTot > 1.32) && (_nTisTot <= 1.54)): {					
						(_displayUI displayCtrl 1124) ctrlSetText "modules\trindisplay\images\tis_6.paa";					
						};
						case ((_nTisTot > 1.54) && (_nTisTot <= 1.96)): {					
						(_displayUI displayCtrl 1124) ctrlSetText "modules\trindisplay\images\tis_7.paa";					
						};
						case ((_nTisTot > 1.96) && (_nTisTot <= 2.15)): {					
						(_displayUI displayCtrl 1124) ctrlSetText "modules\trindisplay\images\tis_8.paa";					
						};
						case ((_nTisTot > 2.15) && (_nTisTot <= 2.38)): {
						//playSound "trin_dispWarn";
						(_displayUI displayCtrl 1124) ctrlSetText "modules\trindisplay\images\tis_9.paa";					
						};
						case (_nTisTot > 2.38): {
						playSound "trin_dispWarn";	
						(_displayUI displayCtrl 1124) ctrlSetText "modules\trindisplay\images\tis_9.paa";					
						};	
			};
						
			sleep 1; //---DO NOT CHANGE --- All time based calculations dependent on value.			
			
			_depthB = (((getPosASL _unit) select 2)* -3.28);
			_pressureB = ((_depthB / 33) + 1);
			_dPressure = (_pressureB - _pressureA)/60;			
			_dDepth = _depthA - _depthB;
			_narcColor = ppEffectCreate ["colorCorrections", 1001];
			_narcBlur = ppEffectCreate ["radialBlur", 1002];
			_narcChroma = ppEffectCreate ["chromAberration", 1003];
			
			
			//Simulate Tissue Saturation eg. _nTisTot = _pNAlv + _rNAmb *((_diveTime/60) -(1/_tisK)) - (_pNAlv -0.736 - (_rNAmb/_tisK)) *exp (_tisConst) --Schreiner Equation--
			switch (true) do
			{
					case (_depthB > _depthA): {						 
						 _nTisTot = [_percN2, _tisK, _pressure, _dPressure, _diveTime] call getTisTot;
						 _HeTisTot = [_percHe, _tisK, _pressure, _dPressure, _diveTime] call getTisTot;
						 _O2TisTot = [_percO2, _tisK, _pressure, _dPressure, _diveTime] call getTisTot;
						_tempC = _tempC + (_dDepth *0.0004);
						_AscCeilB = _AscCeilA + 0.00001;
						};
					case (_depthA > _depthB): {
						_nTisTot = [_percN2, _tisK, _pressure, _dPressure, _diveTime] call getTisTot;
						_HeTisTot = [_percHe, _tisK, _pressure, _dPressure, _diveTime] call getTisTot;
						_O2TisTot = [_percO2, _tisK, _pressure, _dPressure, _diveTime] call getTisTot;
						_AscCeilB = 0;						
						};
					case (_depthA == _depthB): {
						_nTisTot = [_percN2, _tisK, _pressure, _dPressure, _diveTime] call getTisTot;
						_HeTisTot = [_percHe, _tisK, _pressure, _dPressure, _diveTime] call getTisTot;
						_O2TisTot = [_percO2, _tisK, _pressure, _dPressure, _diveTime] call getTisTot;
						_AscCeilB = _AscCeilA + 0.00001;
						};	
			};

			
			
			//Calculate Max Depth so far
			if ((_depthB > _depthA) && (_maxDepth < _depthB)) then 
			{
				_maxDepth = _depthB;			
			};	
			
			//Set Ascent Ceiling			
			if (_AscCeil < _AscCeilB) then 
			{
				 _AscCeil = (round(_AscCeilB /10)) *10;
				 
			};
			
			//Init Deco Profile
			if((_AscCeil > 10) && (_doDeco < 1)) then 
			{				
				_decoTime = _AscCeil *(round(_totalTis *10));
				_deepStopCeil = _maxDepth /2;				
				_deepStopTime = _AscCeil *(round(_totalTis *5));
				_doDeco = _doDeco + 1;
				_diveCnt = 1;
				playSound "trin_dispStart";
			};
			
			// Start deco countdown once unit is with range
			if ((_diveCnt > 0) && (_depth2deco < 3)) then
			{
				_decoTime = _decoTime - 1;
				playSound "trin_dispCount";
			};			
			
			//Stop deco countdown and reset timer
			if(_decoTime == 1) then
			{
				_decoTime = 0;
				_AscCeil = 0;
				_doDeco = 0;
				playSound "trin_dispClear";
			};
			
			// Start deep stop countdown once unit is with range
			if ((_diveCnt > 0) && (_depth2deepStop < 3) && (_depth2deepStop > -3)) then
			{
				_deepStopTime = _deepStopTime - 1;
				playSound "trin_dispCount";
			};
			
			//Stop deep stop countdown and reset timer
			if(_deepStopTime == 1) then
			{
				_deepStopTime = 0;
				_deepStopCeil = 0;
				playSound "trin_dispClear";
				
			};
			
			//Narcotic Effects kick in if toxicity threshold passed. --WIP--
			switch (true) do
			{
				case ((_narcFactor > 9) && (_affectedColor == 0)): {									
					_narcColor ppEffectEnable true;
					_narcColor ppEffectAdjust [_narcEffectNeg, 1, 0, [0,0,0,0.5], [0,0,0,0.5], [0,0,0,0]];
					_narcBlur ppEffectAdjust [0.08,0.08,0.05,0.05];					
					_narcColor ppEffectCommit 10;					
					_affectedColor = 1;				
					};
				case ((_narcFactor < 9) && (_affectedColor ==1)): {					
						ppEffectDestroy _narcColor;
						_affectedColor = 0;	
					};
				case ((_narcFactor > 7) && (_affectedBlur == 0)) : {									
					_narcBlur ppEffectEnable true;
					_narcBlur ppEffectAdjust [0.01,0.01,0.15,0.15];
					_narcColor ppEffectAdjust [_narcEffectNeg, 1, 0, [0,0,0,0.5], [0,0,0,0.5], [0,0,0,0]];
					_narcChroma ppEffectAdjust [0.12,0.12,true];
					_narcBlur ppEffectCommit 30;
					_affectedBlur = 1;					
					};
				case ((_narcFactor < 7) && (_affectedBlur == 1)) : {									
					ppEffectDestroy _narcBlur;
					_affectedBlur = 0;
					};	
				case ((_narcFactor > 4) && (_affectedChroma == 0)) : {									
					_narcChroma ppEffectEnable true;
					_narcChroma ppEffectAdjust [0.06,0.06,true];
					_narcColor ppEffectAdjust [_narcEffectNeg, 1, 0, [0,0,0,0.5], [0,0,0,0.5], [0,0,0,0]];
					_narcChroma ppEffectCommit 30;
					_affectedChroma = 1;
					};			
				case ((_narcFactor < 4) && (_affectedChroma == 1)) : {									
					ppEffectDestroy _narcChroma;
					_affectedChroma = 0;	
					};	
			};
			
			//Calculate ascending time and temperature change at depth					
			if ((_depthA > _depthB) && (_dDepth > 1)) then 
			{
				_ascTime = _depth / _dDepth;
				_tempC = _tempC + (_dDepth *0.0004);
			}
				else
					{
					_ascTime = 0;
					_tempC = _tempC
					};
			
			//Check if player surpasses Maximum Operating Depth
			if (_depth > _maxOD) then
			{
				playSound "trin_dispWarn";
			};
			
			//Prevent huge numbers from clogging diaplay
			if(_ascTime > 600) then 
			{
			
				_ascTime = 0;
				
			};
			
			// Running out of air results in tanks being discarded.
			if (_psi < 0) then
            {
                removeVest _unit;
            };		
			
	};
		
	2 cutText ["","PLAIN"];
				
 };
};
