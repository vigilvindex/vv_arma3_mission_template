class trin_dispStart
{
    name = "Trin Display Start";
    sound[] = {"modules\trindisplay\sounds\dispStart.ogg", 1, 1};
    titles[] = {};
};
class trin_dispExit
{
    name = "Trin Display Exit";
    sound[] = {"modules\trindisplay\sounds\dispExit.ogg", 1, 1};
    titles[] = {};
};
class trin_dispWarn
{
    name = "Trin Display Warn";
    sound[] = {"modules\trindisplay\sounds\dispWarn.ogg", 0.5, 1};
    titles[] = {};
};
class trin_dispCount
{
    name = "Trin Display Count";
    sound[] = {"modules\trindisplay\sounds\dispCount.ogg", 2, 1};
    titles[] = {};
};
class trin_dispClear
{
    name = "Trin Display Clear";
    sound[] = {"modules\trindisplay\sounds\dispClear.ogg", 1, 1};
    titles[] = {};
};
class trin_dispSilence
{
    name = "Trin Display Silence";
    sound[] = {"modules\trindisplay\sounds\dispSilence.ogg", 1, 1};
    titles[] = {};
};