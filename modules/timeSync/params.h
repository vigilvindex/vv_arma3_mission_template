class DateTimeSync { 
    title       = "Time - Sync Time:";
    values[]    = {0,1};
    texts[]     = {"Off","On"};
    default     = 1;
};