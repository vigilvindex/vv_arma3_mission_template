class TpwCasSwitch { 
    title       = "AI - TPWCAS:";
    values[]    = {0,1};
    texts[]     = {"Disabled","Enabled"};
    default     = 1;
};
class TpwCasDebugSwitch { 
    title       = "AI - TPWCAS - Debug:";
    values[]    = {0,1};
    texts[]     = {"Disabled","Enabled"};
    default     = 0;
};
class TpwCasSkillSwitch { 
    title       = "AI - TPWCAS - Skill Modifier:";
    values[]    = {0,1};
    texts[]     = {"Disabled","Enabled"};
    default     = 0;
};
class TpwCasLosSwitch { 
    title       = "AI - TPWCAS - Line Of Sight:";
    values[]    = {0,1};
    texts[]     = {"Disabled","Enabled"};
    default     = 1;
};
class TpwCasShakeSwitch { 
    title       = "AI - TPWCAS - Shake While Suppressed:";
    values[]    = {0,1};
    texts[]     = {"Disabled","Enabled"};
    default     = 0;
};
class TpwCasVisSwitch { 
    title       = "AI - TPWCAS - Visual Impairment While Suppressed:";
    values[]    = {0,1};
    texts[]     = {"Disabled","Enabled"};
    default     = 0;
};