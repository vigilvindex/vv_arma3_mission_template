class TawVDSwitch { 
    title       = "Visual - View Distance Settings:";
    values[]    = {0,1};
    texts[]     = {"Disabled","Enabled"};
    default     = 1;
};
