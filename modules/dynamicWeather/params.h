class WeatherDynamicWeather { 
    title       = "Weather - Dynamic Weather:";
    values[]    = {0,1};
    texts[]     = {"Off","On"};
    default     = 1;
};
class WeatherFog { 
    title       = "Weather - Initial Fog:";
    values[]    = {0,5,25,50,75,100,-100};
    texts[]     = {"None","Very Low","Low","Medium","High","Maximum","Random"};
    default     = 0;
};
class WeatherOvercast { 
    title       = "Weather - Initial Overcast:";
    values[]    = {0,5,25,50,75,100,-100};
    texts[]     = {"None","Very Low","Low","Medium","High","Maximum","Random"};
    default     = 0;
};
class WeatherRain { 
    title       = "Weather - Initial Rain:";
    values[]    = {0,5,25,50,75,100,-100};
    texts[]     = {"None","Very Low","Low","Medium","High","Maximum","Random"};
    default     = 0;
};
class WeatherWind { 
    title       = "Weather - Initial Wind:";
    values[]    = {0,5,25,50,75,100,-100};
    texts[]     = {"None","Very Low","Low","Medium","High","Maximum","Random"};
    default     = 0;
};
class WeatherTimeMinChange { 
    title       = "Weather - Minimum Change Time:";
    values[]    = {1,5,10,15,25,30,45,60,99999};
    texts[]     = {"1 Minute","5 Minutes","10 Minutes","15 Minutes","25 Minutes","30 Minutes","45 Minutes","1 Hour","Never"};
    default     = 30;
};
class WeatherTimeMaxChange { 
    title       = "Weather - Maximum Change Time:";
    values[]    = {1,5,10,15,25,30,45,60,99999};
    texts[]     = {"1 Minute","5 Minutes","10 Minutes","15 Minutes","25 Minutes","30 Minutes","45 Minutes","1 Hour","Never"};
    default     = 60;
};
class WeatherTimeMinChangeBet { 
    title       = "Weather - Minimum Interval Time:";
    values[]    = {1,5,10,15,25,30,45,60,99999};
    texts[]     = {"1 Minute","5 Minutes","10 Minutes","15 Minutes","25 Minutes","30 Minutes","45 Minutes","1 Hour","Never"};
    default     = 30;
};
class WeatherTimeMaxChangeBet { 
    title       = "Weather - Maximum Interval Time:";
    values[]    = {1,5,10,15,25,30,45,60,99999};
    texts[]     = {"1 Minute","5 Minutes","10 Minutes","15 Minutes","25 Minutes","30 Minutes","45 Minutes","1 Hour","Never"};
    default     = 60;
};
class WeatherFogMin { 
    title       = "Weather - Minimum Fog Level:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10};
    texts[]     = {"0%","10%","20%","30%","40%","50%","60%","70%","80%","90%","100%"};
    default     = 0;
};
class WeatherFogMax { 
    title       = "Weather - Maximum Fog Level:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10};
    texts[]     = {"0%","10%","20%","30%","40%","50%","60%","70%","80%","90%","100%"};
    default     = 6;
};
class WeatherOcastMin { 
    title       = "Weather - Minimum Overcast Level:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10};
    texts[]     = {"0%","10%","20%","30%","40%","50%","60%","70%","80%","90%","100%"};
    default     = 0;
};
class WeatherOcastMax { 
    title       = "Weather - Maximum Overcast Level:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10};
    texts[]     = {"0%","10%","20%","30%","40%","50%","60%","70%","80%","90%","100%"};
    default     = 8;
};
class WeatherRainMin { 
    title       = "Weather - Minimum Rain Level:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10};
    texts[]     = {"0%","10%","20%","30%","40%","50%","60%","70%","80%","90%","100%"};
    default     = 0;
};
class WeatherRainMax { 
    title       = "Weather - Maximum Rain Level:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10};
    texts[]     = {"0%","10%","20%","30%","40%","50%","60%","70%","80%","90%","100%"};
    default     = 8;
};
class WeatherRainOnce { 
    title       = "Weather - Rain Only For One Consecutive Interval:";
    values[]    = {0,1};
    texts[]     = {"Off","On"};
    default     = 0;
};
class WeatherRainChangeProb { 
    title       = "Weather - Rain Change Probablility:";
    values[]    = {0,25,50,75,100};
    texts[]     = {"0%","25%","50%","75%","100%"};
    default     = 25;
};
class WeatherRainMinInt { 
    title       = "Weather - Minimum Rain Interval Time:";
    values[]    = {0,1,5,10,15,25,30,45,60,99999};
    texts[]     = {"Disabled","1 Minute","5 Minutes","10 Minutes","15 Minutes","25 Minutes","30 Minutes","45 Minutes","1 Hour","Never"};
    default     = 0;
};
class WeatherWindMin { 
    title       = "Weather - Minimum Wind Level:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10};
    texts[]     = {"0%","10%","20%","30%","40%","50%","60%","70%","80%","90%","100%"};
    default     = 0;
};
class WeatherWindMax { 
    title       = "Weather - Maximum Wind Level:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10};
    texts[]     = {"0%","10%","20%","30%","40%","50%","60%","70%","80%","90%","100%"};
    default     = 8;
};
class WeatherWindChangeProb { 
    title       = "Weather - Wind Change Probablility:";
    values[]    = {0,25,50,75,100};
    texts[]     = {"0%","25%","50%","75%","100%"};
    default     = 25;
};
