class SlpSpawnSwitch { 
    title       = "AI - SLP Spawn:";
    values[]    = {0,1};
    texts[]     = {"Off","On"};
    default     = 1;
};
class SlpSpawnDebug { 
    title       = "AI - SLP Spawn - Debug:";
    values[]    = {0,1};
    texts[]     = {"Off","On"};
    default     = 0;
};
class SlpSpawnInfSkill { 
    title       = "AI - SLP Spawn - Infantry Skill:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10"};
    default     = 7;
};
class SlpSpawnVehSkill { 
    title       = "AI - SLP Spawn - Vehicle Skill:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10"};
    default     = 8;
};
class SlpSpawnAirSkill { 
    title       = "AI - SLP Spawn - Air Skill:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10"};
    default     = 9;
};
class SlpSpawnInfGrpNum { 
    title       = "AI - SLP Spawn - Infantry Groups:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","20","30","40","50","60","70","80","90","100"};
    default     = 10;
};
class SlpSpawnInfGrpSize { 
    title       = "AI - SLP Spawn - Infantry Group Size:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,11,12};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","11","12"};
    default     = 5;
};
class SlpSpawnVehGrpNum { 
    title       = "AI - SLP Spawn - Vehicle Groups:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","20","30","40","50","60","70","80","90","100"};
    default     = 0;
};
class SlpSpawnVehGrpSize { 
    title       = "AI - SLP Spawn - Vehicle Group Size:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,11,12};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","11","12"};
    default     = 0;
};
class SlpSpawnArmGrpNum { 
    title       = "AI - SLP Spawn - Armor Groups:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","20","30","40","50","60","70","80","90","100"};
    default     = 0;
};
class SlpSpawnArmGrpSize { 
    title       = "AI - SLP Spawn - Armor Group Size:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,11,12};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","11","12"};
    default     = 0;
};
class SlpSpawnAirGrpNum { 
    title       = "AI - SLP Spawn - Air Groups:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","20","30","40","50","60","70","80","90","100"};
    default     = 0;
};
class SlpSpawnNavInfGrpSize { 
    title       = "AI - SLP Spawn - Naval Infantry Group Size:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,11,12};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","11","12"};
    default     = 5;
};
class SlpSpawnNavInfGrpNum { 
    title       = "AI - SLP Spawn - Naval Infantry Groups:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","20","30","40","50","60","70","80","90","100"};
    default     = 2;
};
class SlpSpawnNavVehGrpSize { 
    title       = "AI - SLP Spawn - Naval Vehicle Group Size:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,11,12};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","11","12"};
    default     = 0;
};
class SlpSpawnNavVehGrpNum { 
    title       = "AI - SLP Spawn - Naval Vehicle Groups:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","20","30","40","50","60","70","80","90","100"};
    default     = 0;
};