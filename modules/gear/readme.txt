Hello! Thank you for using my Virtual Ammobox System (VAS). This was created for a specific mission to cut back on network lag & ammo box usage and well decided to release for public!

System is very easy to use, to add this to your mission copy the gear folder & data folder from the scripts folder to your mission folder, edito description.ext and put:

#include "modules\gear\common.hpp"
#include "modules\gear\menu.hpp"

Somewhere in description.ext
This shouldn't conflict with any other dialogs unless you are trying to use this in Wasteland (as the class names for the dialogs are the same as wasteland).
It is best to attach the action to a pre-existing ammo box so place a ammo box on the map via editor and in the initialization field put:

this addAction["<t color='#ff1111'>Gear Menu</t>", "modules\gear\open.sqf"];

And your done! Just look at the ammo box, scroll and click Virtual Ammobox. The interface is easy to use so have fun!

Known Issue(s)/Bug(s):
NONE!!!

Changelog: v0.3
Fixed: Magazines were duped in the player side list.
Changed: Entire UI! Yay it now looks like A3 UI (Thanks SaMatra for helping!)

Credits & thanks:
Kronzky - For his string functions library
SaMatra - For helping with the UI Conversion from OA->A3