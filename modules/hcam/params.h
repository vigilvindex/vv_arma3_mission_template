class HcamSwitch { 
    title       = "Helmet Camera Live Feed:";
    values[]    = {0,1};
    texts[]     = {"Off","On"};
    default     = 1;
};