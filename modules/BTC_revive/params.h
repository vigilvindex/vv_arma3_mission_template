class BtcReviveSwitch { 
    title       = "Revive - BTC Revive:";
    values[]    = {0,1};
    texts[]     = {"Off","On"};
    default     = 1;
};
class BtcReviveButton { 
    title       = "Revive - BTC Revive - Allow Head Movement While Injured:";
    values[]    = {0,1};
    texts[]     = {"Disabled","Enabled"};
    default     = 0;
};
class BtcReviveMin { 
    title       = "Revive - BTC Revive - Minimum Revive Time:";
    values[]    = {5,30,60,120,180,300,600};
    texts[]     = {"5 Seconds","30 Seconds","1 Minute","2 Minutes","3 Minutes","5 Minutes","10 Minutes"};
    default     = 30;
};
class BtcReviveMax { 
    title       = "Revive - BTC Revive - Maximum Revive Time:";
    values[]    = {5,30,60,120,180,300,600};
    texts[]     = {"5 Seconds","30 Seconds","1 Minute","2 Minutes","3 Minutes","5 Minutes","10 Minutes"};
    default     = 300;
};
class BtcReviveLives { 
    title       = "Revive - BTC Revive - Lives:";
    values[]    = {1,5,10,1000};
    texts[]     = {"One","Five","Ten","Infinite"};
    default     = 1000;
};
class BtcReviveBlackScrn { 
    title       = "Revive - BTC Revive - Black Revive Screen:";
    values[]    = {0,1};
    texts[]     = {"Disabled","Enabled"};
    default     = 0;
};
class BtcReviveAidRevive { 
    title       = "Revive - BTC Revive - First Aid Pack Revive Requirement:";
    values[]    = {0,1};
    texts[]     = {"Disabled","Enabled"};
    default     = 0;
};
class BtcReviveRespawnSwitch { 
    title       = "Revive - BTC Revive - Respawn:";
    values[]    = {0,1};
    texts[]     = {"On","Off"};
    default     = 0;
};
class BtcReviveRespawnTime { 
    title       = "Revive - BTC Revive - Respawn Time:";
    values[]    = {1,5,10,30,60};
    texts[]     = {"1 Second","5 Seconds","10 Seconds","30 Seconds","1 Minute"};
    default     = 5;
};
class BtcReviveGearSwitch { 
    title       = "Revive - BTC Revive - Respawn With Gear:";
    values[]    = {0,1};
    texts[]     = {"Off","On"};
    default     = 1;
};
class BtcRevivePvpSwitch { 
    title       = "Revive - BTC Revive - Enemy Can Revive:";
    values[]    = {0,1};
    texts[]     = {"On","Off"};
    default     = 0;
};
class BtcReviveInjMarkSwitch { 
    title       = "Revive - BTC Revive - Injured Markers:";
    values[]    = {0,1};
    texts[]     = {"Off","On"};
    default     = 1;
};