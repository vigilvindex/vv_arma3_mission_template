/*
Created by =BTC= Giallustio
version 0.7
Visit us at: 
http://www.blacktemplars.altervista.org/
06/03/2012
*/
for [{_i = 0},{_i < count(paramsArray)},{_i = _i + 1}] do {
  call compile format ["%1 = %2",(configName ((missionConfigFile >> "Params") select _i)),(paramsArray select _i)];
};
////////////////// EDITABLE \\\\\\\\\\\\\\\\\\\\\\\\\\
BTC_revive_time_min = (BtcReviveMin);
BTC_revive_time_max = (BtcReviveMax);
BTC_who_can_revive  = ["Man"];
BTC_loop_check      = 0;
BTC_disable_respawn = (BtcReviveRespawnSwitch);
BTC_respawn_gear    = (BtcReviveGearSwitch);
BTC_active_lifes    = 1;
BTC_lifes           = 10;
BTC_black_screen    = (BtcReviveBlackScrn);//Black screen + button while unconscious or action wheel and clear view
BTC_action_respawn  = (BtcReviveButton);//if black screen is set to 0 you can choose if you want to use the action wheel or the button. Keep in mind that if you don't use the button, the injured player can use all the action, frag too....
BTC_respawn_time    = (BtcReviveRespawnTime);
BTC_active_mobile   = 0;//Active mobile respawn (You have to put in map the vehicle and give it a name. Then you have to add one object per side to move to the mobile (BTC_base_flag_west,BTC_base_flag_east) - (1 = yes, 0 = no))
BTC_mobile_respawn  = 0;//Active the mobile respawn fnc (1 = yes, 0 = no)
BTC_mobile_respawn_time = 30;//Secs delay for mobile vehicle to respawn
BTC_need_first_aid = (BtcReviveAidRevive);//You need a first aid kit to revive (1 = yes, 0 = no)
BTC_pvp = (BtcRevivePvpSwitch); //(disable the revive option for the enemy)
BTC_injured_marker = (BtcReviveInjMarkSwitch);
BTC_vehs_mobile_west = [];//Editable - define mobile west
BTC_vehs_mobile_east = [];//Editable - define mobile east
BTC_vehs_mobile_guer = [];//Editable - define mobile independent
////////////////// Don't edit below \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

//FNC
call compile preprocessFile "modules\BTC_revive\BTC_functions.sqf";

if (isServer) then
{
	//Mobile
	if (BTC_active_mobile == 1 && count BTC_vehs_mobile_west != 0) then {for "_i" from 0 to ((count BTC_vehs_mobile_west) - 1) do {_veh = (BTC_vehs_mobile_west select _i);_var = str (_veh);_veh setVariable ["BTC_mobile_west",_var,true];if (BTC_mobile_respawn == 1) then {_resp = [_veh,_var,"BTC_mobile_west"] spawn BTC_vehicle_mobile_respawn;};};} else {{deleteVehicle _x} foreach BTC_vehs_mobile_west;};
	if (BTC_active_mobile == 1 && count BTC_vehs_mobile_east != 0) then {for "_i" from 0 to ((count BTC_vehs_mobile_east) - 1) do {_veh = (BTC_vehs_mobile_east select _i);_var = str (_veh);_veh setVariable ["BTC_mobile_east",_var,true];if (BTC_mobile_respawn == 1) then {_resp = [_veh,_var,"BTC_mobile_east"] spawn BTC_vehicle_mobile_respawn;};};} else {{deleteVehicle _x} foreach BTC_vehs_mobile_east;};
	if (BTC_active_mobile == 1 && count BTC_vehs_mobile_guer != 0) then {for "_i" from 0 to ((count BTC_vehs_mobile_guer) - 1) do {_veh = (BTC_vehs_mobile_guer select _i);_var = str (_veh);_veh setVariable ["BTC_mobile_guer",_var,true];if (BTC_mobile_respawn == 1) then {_resp = [_veh,_var,"BTC_mobile_guer"] spawn BTC_vehicle_mobile_respawn;};};} else {{deleteVehicle _x} foreach BTC_vehs_mobile_guer;};
	BTC_killed_pveh = [];publicVariable "BTC_killed_pveh";
	BTC_drag_pveh = [];publicVariable "BTC_drag_pveh";
	BTC_marker_pveh = [];publicVariable "BTC_marker_pveh";
};
if (isDedicated) exitWith {};

BTC_dragging = false;
BTC_respawn_cond = false;
//Init
[] spawn
{
	waitUntil {!isNull player};
	waitUntil {player == player};
	"BTC_drag_pveh" addPublicVariableEventHandler BTC_fnc_PVEH;
	"BTC_marker_pveh" addPublicVariableEventHandler BTC_fnc_PVEH;
	"BTC_killed_pveh" addPublicVariableEventHandler BTC_fnc_PVEH;
	player addRating 9999;
	BTC_side = side player;
	BTC_respawn_marker = format ["respawn_%1",side player];
	if (BTC_respawn_gear == 1) then {player addEventHandler ["HandleDamage", BTC_fnc_handledamage];};
	player addEventHandler ["HandleDamage", BTC_fnc_handledamage];
	player addEventHandler ["Killed", BTC_player_killed];
	player setVariable ["BTC_need_revive",0,true];
	if ([player] call BTC_is_class_can_revive) then {player addAction [("<t color=""#ED2744"">") + ("First aid") + "</t>","modules\BTC_revive\BTC_addAction.sqf",[[],BTC_first_aid], 8, true, true, "", "[] call BTC_check_action_first_aid"];};
	player addAction [("<t color=""#ED2744"">") + ("Drag") + "</t>","modules\BTC_revive\BTC_addAction.sqf",[[],BTC_drag], 8, true, true, "", "[] call BTC_check_action_drag"];
	if (BTC_active_mobile == 1) then 
	{
		switch (true) do
		{
			case (side player == west) : {{_spawn = [str (_x)] spawn BTC_mobile_marker;BTC_base_flag_west addAction [("<t color=""#ED2744"">") + ("Move to mobile " + format ["%1",_x]) + "</t>","modules\BTC_revive\BTC_addAction.sqf",[[str (_x)],BTC_move_to_mobile], 8, true, true, "", format ["[str (%1)] call BTC_mobile_check",_x]];} foreach BTC_vehs_mobile_west;};
			case (side player == east) : {{_spawn = [str (_x)] spawn BTC_mobile_marker;BTC_base_flag_east addAction [("<t color=""#ED2744"">") + ("Move to mobile " + format ["%1",_x]) + "</t>","modules\BTC_revive\BTC_addAction.sqf",[[str (_x)],BTC_move_to_mobile], 8, true, true, "", format ["[str (%1)] call BTC_mobile_check",_x]];} foreach BTC_vehs_mobile_east;};
			case (side player == guer) : {{_spawn = [str (_x)] spawn BTC_mobile_marker;BTC_base_flag_guer addAction [("<t color=""#ED2744"">") + ("Move to mobile " + format ["%1",_x]) + "</t>","modules\BTC_revive\BTC_addAction.sqf",[[str (_x)],BTC_move_to_mobile], 8, true, true, "", format ["[str (%1)] call BTC_mobile_check",_x]];} foreach BTC_vehs_mobile_guer;};
		};
	};
	BTC_gear = [] call BTC_get_gear;
	if (BTC_loop_check == 1) then {[] spawn BTC_revive_loop;};
	//[] spawn {while {true} do {sleep 0.5;hintSilent format ["%1",BTC_gear];};};
	//hint "REVIVE STARTED";
};