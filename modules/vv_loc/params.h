class VVLocTownBuildingInfGrpNum { 
    title       = "AI - VVLoc Town Buildings - Infantry Groups:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","20","30","40","50","60","70","80","90","100"};
    default     = 5;
};
class VVLocTownBuildingInfGrpSize { 
    title       = "AI - VVLoc Town Buildings - Infantry Group Size:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,11,12};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","11","12"};
    default     = 2;
};
class VVLocTownPatrolInfGrpNum { 
    title       = "AI - VVLoc Town Patrols - Infantry Groups:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","20","30","40","50","60","70","80","90","100"};
    default     = 2;
};
class VVLocTownPatrolInfGrpSize { 
    title       = "AI - VVLoc Town Patrols - Infantry Group Size:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,11,12};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","11","12"};
    default     = 5;
};
class VVLocTownPatrolVehGrpNum { 
    title       = "AI - VVLoc Town Patrols - Vehicle Groups:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","20","30","40","50","60","70","80","90","100"};
    default     = 1;
};
class VVLocTownPatrolVehGrpSize { 
    title       = "AI - VVLoc Town Patrols - Vehicle Group Size:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,11,12};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","11","12"};
    default     = 2;
};
class VVLocTownPatrolArmGrpNum { 
    title       = "AI - VVLoc Town Patrols - Armor Groups:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","20","30","40","50","60","70","80","90","100"};
    default     = 0;
};
class VVLocTownPatrolArmGrpSize { 
    title       = "AI - VVLoc Town Patrols - Armor Group Size:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,11,12};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","11","12"};
    default     = 0;
};
class VVLocTownPatrolAirGrpNum { 
    title       = "AI - VVLoc Town Patrols - Air Groups:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","20","30","40","50","60","70","80","90","100"};
    default     = 0;
};
class VVLocTownPatrolAirGrpSize { 
    title       = "AI - VVLoc Town Patrols - Air Group Size:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,11,12};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","11","12"};
    default     = 0;
};
class VVLocCampBuildingInfGrpNum { 
    title       = "AI - VVLoc Camp Buildings - Infantry Groups:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","20","30","40","50","60","70","80","90","100"};
    default     = 5;
};
class VVLocCampBuildingInfGrpSize { 
    title       = "AI - VVLoc Camp Buildings - Infantry Group Size:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,11,12};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","11","12"};
    default     = 2;
};
class VVLocCampPatrolInfGrpNum { 
    title       = "AI - VVLoc Camp Patrols - Infantry Groups:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","20","30","40","50","60","70","80","90","100"};
    default     = 2;
};
class VVLocCampPatrolInfGrpSize { 
    title       = "AI - VVLoc Camp Patrols - Infantry Group Size:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,11,12};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","11","12"};
    default     = 5;
};
class VVLocCampPatrolVehGrpNum { 
    title       = "AI - VVLoc Camp Patrols - Vehicle Groups:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","20","30","40","50","60","70","80","90","100"};
    default     = 1;
};
class VVLocCampPatrolVehGrpSize { 
    title       = "AI - VVLoc Camp Patrols - Vehicle Group Size:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,11,12};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","11","12"};
    default     = 2;
};
class VVLocCampPatrolArmGrpNum { 
    title       = "AI - VVLoc Camp Patrols - Armor Groups:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","20","30","40","50","60","70","80","90","100"};
    default     = 0;
};
class VVLocCampPatrolArmGrpSize { 
    title       = "AI - VVLoc Camp Patrols - Armor Group Size:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,11,12};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","11","12"};
    default     = 0;
};
class VVLocCampPatrolAirGrpNum { 
    title       = "AI - VVLoc Camp Patrols - Air Groups:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","20","30","40","50","60","70","80","90","100"};
    default     = 0;
};
class VVLocCampPatrolAirGrpSize { 
    title       = "AI - VVLoc Camp Patrols - Air Group Size:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,11,12};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","11","12"};
    default     = 0;
};
class VVLocRadioBuildingInfGrpNum { 
    title       = "AI - VVLoc Radio Buildings - Infantry Groups:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","20","30","40","50","60","70","80","90","100"};
    default     = 5;
};
class VVLocRadioBuildingInfGrpSize { 
    title       = "AI - VVLoc Radio Buildings - Infantry Group Size:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,11,12};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","11","12"};
    default     = 2;
};
class VVLocRadioPatrolInfGrpNum { 
    title       = "AI - VVLoc Radio Patrols - Infantry Groups:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","20","30","40","50","60","70","80","90","100"};
    default     = 2;
};
class VVLocRadioPatrolInfGrpSize { 
    title       = "AI - VVLoc Radio Patrols - Infantry Group Size:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,11,12};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","11","12"};
    default     = 5;
};
class VVLocRadioPatrolVehGrpNum { 
    title       = "AI - VVLoc Radio Patrols - Vehicle Groups:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","20","30","40","50","60","70","80","90","100"};
    default     = 1;
};
class VVLocRadioPatrolVehGrpSize { 
    title       = "AI - VVLoc Radio Patrols - Vehicle Group Size:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,11,12};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","11","12"};
    default     = 2;
};
class VVLocRadioPatrolArmGrpNum { 
    title       = "AI - VVLoc Radio Patrols - Armor Groups:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","20","30","40","50","60","70","80","90","100"};
    default     = 0;
};
class VVLocRadioPatrolArmGrpSize { 
    title       = "AI - VVLoc Radio Patrols - Armor Group Size:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,11,12};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","11","12"};
    default     = 0;
};
class VVLocRadioPatrolAirGrpNum { 
    title       = "AI - VVLoc Radio Patrols - Air Groups:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","20","30","40","50","60","70","80","90","100"};
    default     = 1;
};
class VVLocRadioPatrolAirGrpSize { 
    title       = "AI - VVLoc Radio Patrols - Air Group Size:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,11,12};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","11","12"};
    default     = 1;
};
class VVLocAirstripBuildingInfGrpNum { 
    title       = "AI - VVLoc Airstrip Buildings - Infantry Groups:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","20","30","40","50","60","70","80","90","100"};
    default     = 5;
};
class VVLocAirstripBuildingInfGrpSize { 
    title       = "AI - VVLoc Airstrip Buildings - Infantry Group Size:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,11,12};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","11","12"};
    default     = 2;
};
class VVLocAirstripPatrolInfGrpNum { 
    title       = "AI - VVLoc Airstrip Patrols - Infantry Groups:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","20","30","40","50","60","70","80","90","100"};
    default     = 2;
};
class VVLocAirstripPatrolInfGrpSize { 
    title       = "AI - VVLoc Airstrip Patrols - Infantry Group Size:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,11,12};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","11","12"};
    default     = 5;
};
class VVLocAirstripPatrolVehGrpNum { 
    title       = "AI - VVLoc Airstrip Patrols - Vehicle Groups:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","20","30","40","50","60","70","80","90","100"};
    default     = 2;
};
class VVLocAirstripPatrolVehGrpSize { 
    title       = "AI - VVLoc Airstrip Patrols - Vehicle Group Size:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,11,12};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","11","12"};
    default     = 2;
};
class VVLocAirstripPatrolArmGrpNum { 
    title       = "AI - VVLoc Airstrip Patrols - Armor Groups:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","20","30","40","50","60","70","80","90","100"};
    default     = 0;
};
class VVLocAirstripPatrolArmGrpSize { 
    title       = "AI - VVLoc Airstrip Patrols - Armor Group Size:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,11,12};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","11","12"};
    default     = 0;
};
class VVLocAirstripPatrolAirGrpNum { 
    title       = "AI - VVLoc Airstrip Patrols - Air Groups:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","20","30","40","50","60","70","80","90","100"};
    default     = 2;
};
class VVLocAirstripPatrolAirGrpSize { 
    title       = "AI - VVLoc Airstrip Patrols - Air Group Size:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,11,12};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","11","12"};
    default     = 2;
};
class VVLocOutpostBuildingInfGrpNum { 
    title       = "AI - VVLoc Outpost Buildings - Infantry Groups:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","20","30","40","50","60","70","80","90","100"};
    default     = 5;
};
class VVLocOutpostBuildingInfGrpSize { 
    title       = "AI - VVLoc Outpost Buildings - Infantry Group Size:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,11,12};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","11","12"};
    default     = 2;
};
class VVLocOutpostPatrolInfGrpNum { 
    title       = "AI - VVLoc Outpost Patrols - Infantry Groups:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","20","30","40","50","60","70","80","90","100"};
    default     = 2;
};
class VVLocOutpostPatrolInfGrpSize { 
    title       = "AI - VVLoc Outpost Patrols - Infantry Group Size:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,11,12};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","11","12"};
    default     = 5;
};
class VVLocOutpostPatrolVehGrpNum { 
    title       = "AI - VVLoc Outpost Patrols - Vehicle Groups:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","20","30","40","50","60","70","80","90","100"};
    default     = 1;
};
class VVLocOutpostPatrolVehGrpSize { 
    title       = "AI - VVLoc Outpost Patrols - Vehicle Group Size:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,11,12};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","11","12"};
    default     = 2;
};
class VVLocOutpostPatrolArmGrpNum { 
    title       = "AI - VVLoc Outpost Patrols - Armor Groups:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","20","30","40","50","60","70","80","90","100"};
    default     = 0;
};
class VVLocOutpostPatrolArmGrpSize { 
    title       = "AI - VVLoc Outpost Patrols - Armor Group Size:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,11,12};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","11","12"};
    default     = 0;
};
class VVLocOutpostPatrolAirGrpNum { 
    title       = "AI - VVLoc Outpost Patrols - Air Groups:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","20","30","40","50","60","70","80","90","100"};
    default     = 0;
};
class VVLocOutpostPatrolAirGrpSize { 
    title       = "AI - VVLoc Outpost Patrols - Air Group Size:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,11,12};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","11","12"};
    default     = 0;
};
class VVLocFarmBuildingInfGrpNum { 
    title       = "AI - VVLoc Farm Buildings - Infantry Groups:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","20","30","40","50","60","70","80","90","100"};
    default     = 5;
};
class VVLocFarmBuildingInfGrpSize { 
    title       = "AI - VVLoc Farm Buildings - Infantry Group Size:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,11,12};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","11","12"};
    default     = 2;
};
class VVLocFarmPatrolInfGrpNum { 
    title       = "AI - VVLoc Farm Patrols - Infantry Groups:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","20","30","40","50","60","70","80","90","100"};
    default     = 2;
};
class VVLocFarmPatrolInfGrpSize { 
    title       = "AI - VVLoc Farm Patrols - Infantry Group Size:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,11,12};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","11","12"};
    default     = 5;
};
class VVLocFarmPatrolVehGrpNum { 
    title       = "AI - VVLoc Farm Patrols - Vehicle Groups:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","20","30","40","50","60","70","80","90","100"};
    default     = 1;
};
class VVLocFarmPatrolVehGrpSize { 
    title       = "AI - VVLoc Farm Patrols - Vehicle Group Size:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,11,12};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","11","12"};
    default     = 2;
};
class VVLocFarmPatrolArmGrpNum { 
    title       = "AI - VVLoc Farm Patrols - Armor Groups:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","20","30","40","50","60","70","80","90","100"};
    default     = 0;
};
class VVLocFarmPatrolArmGrpSize { 
    title       = "AI - VVLoc Farm Patrols - Armor Group Size:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,11,12};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","11","12"};
    default     = 0;
};
class VVLocFarmPatrolAirGrpNum { 
    title       = "AI - VVLoc Farm Patrols - Air Groups:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","20","30","40","50","60","70","80","90","100"};
    default     = 0;
};
class VVLocFarmPatrolAirGrpSize { 
    title       = "AI - VVLoc Farm Patrols - Air Group Size:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,11,12};
    texts[]     = {"None","1","2","3","4","5","6","7","8","9","10","11","12"};
    default     = 0;
};