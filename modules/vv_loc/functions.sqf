﻿/* functions.sqf
 * Description: Functions
 * Author: vigil.vindex@gmail.com
 * Last Updated: 2013/03/22
 * TODO:
 *   - Add validation checks for variables.
 *   - Add more functions.
 */

/* Function: Creates a trigger - http://community.bistudio.com/wiki/createTrigger
 * Returns: Object (trigger)
 * Arguments:
 *   0 - Debug: (boolean) true/false
 *   1 - Name: (string) "TriggerName"
 *   2 - Text: (string) "TriggerText"
 *   3 - Position: (array) [X,Y,Z]
 *   4 - Area: (array) [radius_x,radius_y,angle,rectangle] - http://community.bistudio.com/wiki/setTriggerArea
 *   5 - Activation: (array) [by,type,repeating] - http://community.bistudio.com/wiki/setTriggerActivation
 *   6 - Statements: (array) [condition,activation,deactivation] - http://community.bistudio.com/wiki/setTriggerStatements
 *   7 - Timeout: (array) [min,mid,max,interruptable] - http://community.bistudio.com/wiki/setTriggerTimeout
 * Example:
     _trigger = [ false, "TriggerName", "TriggerText", [10,10,0], [15,15,0,false], ["ANY","PRESENT",true], ["this","hint 'TriggerName Activated'","hint 'TriggerName Deactivated'"], [1,1,1,false] ] spawn custom_function_create_trigger;
 */
custom_function_create_trigger = {
    private ["_debug","_trigger","_trigger_name","_trigger_text","_trigger_position","_trigger_area","_trigger_activation","_trigger_statements","_trigger_timeout"];
    _debug = _this select 0;
    _trigger_name = _this select 1;
    _trigger_text = _this select 2;
    _trigger_position = _this select 3;
    _trigger_area = _this select 4;
    _trigger_activation = _this select 5;
    _trigger_statements = _this select 6;
    _trigger_timeout = _this select 7;
    if (isServer) then {
        _trigger = createTrigger ["EmptyDetector",_trigger_position];
        _trigger setTriggerArea _trigger_area;
        _trigger setTriggerActivation _trigger_activation;
        _trigger setTriggerStatements _trigger_statements;
        _trigger setTriggerTimeout _trigger_timeout;
    };
    _trigger
};

/* Function: Creates a marker - http://community.bistudio.com/wiki/createMarker
 * Returns: Object (marker)
 * Arguments:
 *   0 - Debug: (boolean) true/false
 *   1 - Name: (string) "MarkerName"
 *   2 - Text: (string) "MarkerText" - http://community.bistudio.com/wiki/setMarkerText
 *   3 - Position: (array) [X,Y,Z] - http://community.bistudio.com/wiki/setMarkerPos
 *   4 - Size: (array) [X,Y] - http://community.bistudio.com/wiki/setMarkerSize
 *   5 - Angle: (number) 45 - http://community.bistudio.com/wiki/setMarkerDir
 *   6 - Alpha: (number) 1 - http://community.bistudio.com/wiki/setMarkerAlpha
 *   7 - Color: (string) "Default" - http://community.bistudio.com/wiki/setMarkerColor
 *   8 - Shape: (string) "ELLIPSE" - http://community.bistudio.com/wiki/setMarkerShape
 *   9 - Type: (string) "Dot" - http://community.bistudio.com/wiki/cfgMarkers
 *   10 - Brush: (string) "Border" - http://community.bistudio.com/wiki/setMarkerBrush
 * Example:
 *   _marker = [ false, "MarkerName", "MarkerText", [15,15,0], [15,15], 0, 1, "Default", "ELLIPSE", "Dot", "Border"] spawn custom_function_create_marker;
 */
custom_function_create_marker = {
    private ["_debug","_marker","_marker_name","_marker_x","_marker_y","_marker_shape","_marker_type","_marker_color","_marker_size","_marker_alpha"];
    _debug = _this select 0;
    _marker_name = _this select 1;
    _marker_text = _this select 2;
    _marker_position = _this select 3;
    _marker_size = _this select 4;
    _marker_angle = _this select 5;
    _marker_alpha = _this select 6;
    _marker_color = _this select 7;
    _marker_shape = _this select 8;
    if (_marker_shape == "ICON") then {
        _marker_type = _this select 9;
    }else{
        _marker_brush = _this select 10;
    };
    if (isServer) then {
        _marker = createMarker [_marker_name,_marker_position];
        _marker setMarkerText _marker_text;
        _marker setMarkerSize _marker_size;
        _marker setMarkerDir _marker_angle;
        _marker setMarkerShape _marker_shape;
        _marker setMarkerAlpha _marker_alpha;
        _marker setMarkerColor _marker_color;
        if (_marker_shape == "ICON") then {
            _marker setMarkerType _marker_type;
        }else{
            _marker setMarkerBrush _marker_brush;
        };
    };
    _marker
};

/* Function: Creates an array of 100m resolution grid locations within the area of a given object.
 * Returns: Array [X,Y]
 * Arguments:
 *   0 - Debug: (boolean) true/false
 *   1 - Object: (array) [x,y,radius-x,radius-y]
 * Example:
 *   _grids = [false,[125.145,365.285,100,200]] spawn custom_function_create_grids;
 */
custom_function_create_grids = {
    private ["_array","_debug","_object","_x","_y","_radius_x","_radius_y","_min_x","_min_y","_max_x","_max_y","_loop_x","_loop_y","_current_x","_current_y","_i","_j"];
    _array = [];
    _debug = _this select 0;
    _object = [];
    _object = _this select 1;
    _x = _object select 0;
    _y = _object select 1;
    _radius_x = _object select 2;
    _radius_y = _object select 3;
    _x = 100 * round(_x / 100.0);
    if (_debug) then { diag_log format ["# %1 # custom_function_create_grids : _x = %2",time,_x]; };
    _y = 100 * round(_y / 100.0);
    if (_debug) then { diag_log format ["# %1 # custom_function_create_grids : _y = %2",time,_y]; };
    _radius_x = _radius_x + 50;
    _radius_x = 100 * round(_radius_x / 100.0);
    if (_debug) then { diag_log format ["# %1 # custom_function_create_grids : _radius_x = %2",time,_radius_x]; };
    _radius_y = _radius_y + 50;
    _radius_y = 100 * round(_radius_y / 100.0);
    if (_debug) then { diag_log format ["# %1 # custom_function_create_grids : _radius_y = %2",time,_radius_y]; };
    _min_x = _x - (_radius_x / 2);
    _min_y = _y - (_radius_y / 2);
    _max_x = _x + (_radius_x / 2);
    _max_y = _y + (_radius_y / 2);
    _loop_x = (_max_x - _min_x) / 100;
    _loop_y = (_max_y - _min_y) / 100;
    _current_x = _min_x;
    for [{_i = 0;},{_i <= _loop_x;},{_i = _i + 1;}] do {
        _current_y = _min_y;
        for [{_j = 0;},{_j <= _loop_y;},{_j = _j + 1;}] do {
            _array set [count _array,[(_current_x),(_current_y)]];
            _current_y = _current_y + 100;
        };
        _current_x = _current_x + 100;
    };
    if (_debug) then {
        diag_log format ["# %1 # custom_function_create_grids : _array = %2",time,count _array];
        { diag_log format ["# %1 # custom_function_create_grids : %2",time,_x]; } forEach _array;
    };
    _array
};