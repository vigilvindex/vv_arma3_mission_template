﻿/* getMap.sqf
 * Description: Checks the map name and returns an array of locations for that map.
 * Author: vigil.vindex@gmail.com
 * Last Updated: 2013/03/22
 * Returns: array [NAME,TYPE,POSITION[x,y,z],RADIUS-X,RADIUS-Y,ANGLE]
 * Arguments:
 *   0 - Debug: (boolean) true/false
 * Example: _locations_array = [true] execVM "getMap.sqf";
 * TODO:
 *   - Add locations for Altis.
 *   - Add fuel locations for Stratis.
 *   - Add validation checks for arguments.
 *   - Generate data rather than manually.
 */
// Create Variables
private ["_debug","_worldName","_locations"];
_debug = _this select 0;
// Get World Name
_worldName = worldName;
if (_debug) then {diag_log format ["# %1 # getMap.sqf : _worldName = %2",time,_worldName];};
// Get Locations
switch (toLower(_worldName)) do {
    case "altis": {};
    case "stratis": {
        _locations = [
            // Towns
            ["jay_cove","town",[2460.9453,2.8346214,1145.1851],100,100,0],
            ["agios_cephas","town",[2788.6455,143.29987,1741.2759],100,100,0],
            ["agios_ioannis","town",[3085.0688,167.83731,2147.7705],100,100,0],
            ["girna","town",[2018.1788,3.8933091,2705.9966],200,200,0],
            ["agia_marina","town",[2988.6865,3.3499999,6027.8672],200,200,0],
            // Camps
            ["maxwell","camp",[3344.0212,228.2285,2962.02],100,100,0],
            ["tempest","camp",[1965.812,2.8351879,3529.6792],100,100,0],
            ["kamino_firing_range","camp",[6454.5986,20.324501,5279.4902],200,200,0],
            ["rogain","camp",[4983.4619,207.15732,5877.5015],100,100,0],
            // Radio Stations
            ["station_mike","radio",[4322.998,225.21544,3812.0386],200,200,0],
            // Airstrips
            ["airstrip","airstrip",[1999.7139,7.0035496,5655.0967],300,300,0],
            // Outposts
            ["old","outpost",[4340.1548,177.35001,4354.7324],100,100,0],
            // Farms
            ["kill","farm",[4457.4521,119.37717,6771.896],100,100,0]
        ];
    };
    case "chernarus": {
        _locations = [
            // Radio Stations
            ["green_mountain","radio",[3708.271,434.96487,5985.4795],25,25,0],
            ["olsha","radio",[12939.948,209.68216,12760.253],25,25,0],
            // Power Stations
            ["chernogorsk","power",[5679.6743,89.16832,2975.9426],15,15,0],
            ["elektrozavodsk","power",[10435.112,17.114872,2614.0796],25,25,0],
            ["zelenogorsk","power",[2272.7102,180.47,5247.7085],15,15,0],
            // Castles
            ["black_mountain","castle",[10285.959,363.9826,12043.881],100,100,0],
            ["devils_castle","castle",[6892.7451,394.41711,11436.609],100,100,0],
            ["rog","castle",[11250.752,292.74026,4281.5215],25,25,0],
            ["zub","castle",[6537.6489,390.47305,5594.9072],25,25,0],
            // Airstrips
            ["balota","airstrip",[4932.7935,9,2452.2649],350,100,30],
            ["north_east","airstrip",[12115.728,159,12681.972],350,150,25],
            ["north_west","airstrip",[4583.5088,339,10323.425],650,250,55],
            // Petrol Stations
            ["elektrozavodsk","fuel",[9509.0518,5.4806337,2006.9761],25,25,0],
            ["chernogorsk_north","fuel",[6699.3584,12.576105,2998.6348],25,25,0],
            ["chernogorsk_south","fuel",[5858.1797,5.0106945,2199.2368],25,25,0],
            ["kamenka","fuel",[2019.3606,5.7929387,2239.012],10,10,0],
            ["novy_sobor","fuel",[7261.1514,293.92615,7658.4141],15,15,0],
            ["solnichniy","fuel",[13383.537,10.13465,6604.6196],25,25,0],
            ["staroye","fuel",[10143.096,237.61275,5305.8481],25,25,0],
            ["zelenogorsk","fuel",[2694.9878,223.12,5600.3931],20,20,0],
            ["vybor","fuel",[3652.5168,317.19913,8973.1357],15,15,0],
            ["pustoshka","fuel",[2998.3545,349.15891,7469.5288],15,15,0],
            ["pogorevka","fuel",[4730.4077,281.42706,6385.21],25,25,0],
            ["grishino","fuel",[5853.2466,277.35999,10107.741],15,15,0],
            ["gorka","fuel",[10433.247,223.07986,8874.084],15,15,0],
            // Factories
            ["berezino","factory",[12751.128,6,9656.1221],100,100,0],
            ["orlovets","factory",[11470.735,233.38654,7486.9785],25,25,0],
            ["solnichniy","factory",[13067.236,5.9929819,7000.4668],250,250,0],
            // Towns
            ["kamenka","town",[1836.9792,6.1426063,2237.9958],225,225,0],
            ["pavlovo","town",[1756.056,139.52258,3795.7944],250,250,0],
            ["komorovo","town",[3656.2554,5.98,2347.7271],300,300,0],
            ["bor","town",[3341.3979,185.08739,3884.2366],200,200,0],
            ["zelenogorsk","town",[2710.0547,201.03577,5262.0894],250,250,0],
            ["drozhino","town",[3358.324,239.87033,4940.2925],100,100,0],
            ["kozlovka","town",[4505.4004,243.4133,4662.1353],200,200,0],
            ["balota","town",[4478.8198,5.9899998,2437.231],150,150,0],
            ["chernogorsk","town",[6702.6001,6,2618.624],650,350,145],
            ["prigorodki","town",[8059.3608,6,3271.051],150,150,0],
            ["elektrozavodsk","town",[10299.726,6,2127.3425],450,550,0],
            ["kamyshovo","town",[12050.472,5.9400454,3590.3428],150,250,0],
            ["tulga","town",[12670.219,169.52124,4435.2969],250,150,0],
            ["solnichniy","town",[13386.277,5.4607701,6204.3384],150,350,0],
            ["nizhnoye","town",[12895.543,5.8016777,8204.4141],250,250,0],
            ["pusta","town",[9168.5566,223.62219,3886.1602],150,150,0],
            ["msta","town",[11303.298,251.96603,5458.936],150,150,0],
            ["staroye","town",[10112.004,245.17986,5508.8901],250,150,0],
            ["nadezhdino","town",[5890.2441,141.69528,4763.5146],250,250,0],
            ["sosnovka","town",[2515.584,301.18048,6359.918],100,100,0],
            ["pulkovo","town",[4920.748,322.77939,5615.7402],25,25,0],
            ["pogorevka","town",[4468.9849,295.95328,6395.8428],150,150,0],
            ["rogovo","town",[4723.3013,270.3277,6787.8623],150,150,0],
            ["myshkino","town",[1983.8428,257.35934,7379.6108],150,150,0],
            ["pustoshka","town",[3086.6694,301.57446,7907.4805],150,250,45],
            ["lopatino","town",[2739.5542,273.99927,9960.5557],150,150,0],
            ["vybor","town",[3828.7405,310.98999,8934.1279],150,150,0],
            ["kabanino","town",[5326.9961,333.66727,8610.125],150,150,0],
            ["stary_sobor","town",[6151.7681,300.96826,7767.0181],350,250,45],
            ["dolina","town",[11208.13,77.003975,6569.8525],100,100,0],
            ["shakhovka","town",[9626.2383,207.82761,6576.6367],100,100,0],
            ["mogilevka","town",[7540.4004,213.99776,5150.915],150,150,0],
            ["vyshnoye","town",[6538.5986,322,6135.8896],150,150,0],
            ["guglovo","town",[8471.3213,363.93582,6663.8013],100,100,0],
            ["novy_sobor","town",[7049.7827,293.99973,7722.4292],150,150,0],
            ["orlovets","town",[12148.229,185.75215,7288.4238],150,150,0],
            ["polana","town",[10692.746,217.01157,8087.3589],150,150,0],
            ["gorka","town",[9577.7383,304.01276,8861.0059],250,150,0],
            ["petrovka","town",[4974.877,181.99001,12515.067],100,100,0],
            ["grishino","town",[6023.0889,268.12076,10326.805],250,250,0],
            ["gvozdno","town",[8558.6074,178.03197,11945.351],150,150,0],
            ["dubrovka","town",[10396.679,141.35553,9711.0283],250,250,0],
            ["berezino","town",[12444.398,5.6385717,9605.5332],950,450,145],
            ["khelm","town",[12301.594,99.544601,10803.327],150,250,0],
            ["krasnostav","town",[11104.877,212.19722,12328.12],250,150,0],
            ["olsha","town",[13357.51,94.175659,12848.82],100,100,0]
        ];
    };
};
if (_debug) then {
    diag_log format ["# %1 # getMap.sqf : _locations = %2",time,count _locations];
    { diag_log format ["# %1 # getMap.sqf : %2",time,_x]; } forEach _locations;
};
// Return array.
_locations