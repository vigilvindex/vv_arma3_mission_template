﻿#ifdef mod_taw_vd
    #include <modules\taw_vd\params.h>
#endif
class VisualServerViewDistance { 
    title       = "Visual - Server View Distance:";
    values[]    = {100,250,500,750,1000,1500,2000,2500,3000,3500,4000,4500,5000,6000,7000,8000,9000,10000};
    texts[]     = {"100m","250m","500m","750m","1km","1.5km","2km","2.5km","3km","3.5km","4km","4.5km","5km","6km","7km","8km","9km","10km"};
    default     = 1000;
};
class VisualServerObjViewDistance { 
    title       = "Visual - Server Object View Distance:";
    values[]    = {100,250,500,750,1000,1500,2000,2500,3000,3500,4000,4500,5000,6000,7000,8000,9000,10000};
    texts[]     = {"100m","250m","500m","750m","1km","1.5km","2km","2.5km","3km","3.5km","4km","4.5km","5km","6km","7km","8km","9km","10km"};
    default     = 750;
};
class VisualServerShadViewDistance { 
    title       = "Visual - Server Shadow View Distance:";
    values[]    = {100,250,500,750,1000,1500,2000,2500,3000,3500,4000,4500,5000,6000,7000,8000,9000,10000};
    texts[]     = {"100m","250m","500m","750m","1km","1.5km","2km","2.5km","3km","3.5km","4km","4.5km","5km","6km","7km","8km","9km","10km"};
    default     = 100;
};
class VisualPlayerViewDistance { 
    title       = "Visual - Player View Distance:";
    values[]    = {100,250,500,750,1000,1500,2000,2500,3000,3500,4000,4500,5000,6000,7000,8000,9000,10000};
    texts[]     = {"100m","250m","500m","750m","1km","1.5km","2km","2.5km","3km","3.5km","4km","4.5km","5km","6km","7km","8km","9km","10km"};
    default     = 3500;
};
class VisualPlayerObjViewDistance { 
    title       = "Visual - Player Object View Distance:";
    values[]    = {100,250,500,750,1000,1500,2000,2500,3000,3500,4000,4500,5000,6000,7000,8000,9000,10000};
    texts[]     = {"100m","250m","500m","750m","1km","1.5km","2km","2.5km","3km","3.5km","4km","4.5km","5km","6km","7km","8km","9km","10km"};
    default     = 2500;
};
class VisualPlayerShadViewDistance { 
    title       = "Visual - Player Shadow View Distance:";
    values[]    = {100,250,500,750,1000,1500,2000,2500,3000,3500,4000,4500,5000,6000,7000,8000,9000,10000};
    texts[]     = {"100m","250m","500m","750m","1km","1.5km","2km","2.5km","3km","3.5km","4km","4.5km","5km","6km","7km","8km","9km","10km"};
    default     = 250;
};
class VisualGrass { 
    title       = "Visual - Grass:";
    values[]    = {50,25,12,6};
    texts[]     = {"Off","Near","Medium","Far"};
    default     = 25;
};
class Spacer1 { 
    title       = " ";
    values[]    = {0};
    texts[]     = {" "};
    default     = 0;
};
class DateYear { 
    title       = "Date - Year:";
    values[]    = {2013,2012,2011,2010,2009,2008,2007,2006,2005,2004,2003,2002,2001,2000,1999,1998,1997,1996,1995,1994,1993,1992,1991,1990,1989,1988,1987,1986,1985,1984,1983,1982,1981,1980,1979,1978,1977,1976,1975,1974,1973,1972,1971,1970,1969};
    texts[]     = {"2013","2012","2011","2010","2009","2008","2007","2006","2005","2004","2003","2002","2001","2000","1999","1998","1997","1996","1995","1994","1993","1992","1991","1990","1989","1988","1987","1986","1985","1984","1983","1982","1981","1980","1979","1978","1977","1976","1975","1974","1973","1972","1971","1970","1969"};
    default     = 2013;
};
class DateMonth { 
    title       = "Date - Month:";
    values[]    = {1,2,3,4,5,6,7,8,9,10,11,12};
    texts[]     = {"January","February","March","April","May","June","July","August","September","October","November","December"};
    default     = 3;
};
class DateDay { 
    title       = "Date - Day:";
    values[]    = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31};
    texts[]     = {"1st","2nd","3rd","4th","5th","6th","7th","8th","9th","10th","11th","12th","13th","14th","15th","16th","17th","18th","19th","20th","21st","22nd","23rd","24th","25th","26th","27th","28th","29th","30th","31st"};
    default     = 15;
};
class TimeHour { 
    title       = "Time - Hour:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23};
    texts[]     = {"00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23"};
    default     = 22;
};
class TimeMinute { 
    title       = "Time - Minutes:";
    values[]    = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59};
    texts[]     = {"00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59"};
    default     = 0;
};
#ifdef mod_timeSync
    #include <modules\timeSync\params.h>
#endif
class Spacer2 { 
    title       = " ";
    values[]    = {0};
    texts[]     = {" "};
    default     = 0;
};
#ifdef mod_dynamicWeather
    #include <modules\dynamicWeather\params.h>
#endif
class Spacer3 { 
    title       = " ";
    values[]    = {0};
    texts[]     = {" "};
    default     = 0;
};
#ifdef mod_BTC_revive
    #include <modules\BTC_revive\params.h>
#endif
class Spacer4 { 
    title       = " ";
    values[]    = {0};
    texts[]     = {" "};
    default     = 0;
};
#ifdef mod_CEP_caching
    #include <modules\CEP_caching\params.h>
#endif
#ifdef mod_SLP_Spawn
    #include <modules\SLP_Spawn\params.h>
#endif
#ifdef mod_tpwcas
    #include <modules\tpwcas\params.h>
#endif
#ifdef mod_UPSMON
    #include <modules\UPSMON\params.h>
#endif
#ifdef mod_vv_loc
    #include <modules\vv_loc\params.h>
#endif
class Spacer5 { 
    title       = " ";
    values[]    = {0};
    texts[]     = {" "};
    default     = 0;
};
#ifdef mod_hcam
    #include <modules\hcam\params.h>
#endif
class Spacer6 { 
    title       = " ";
    values[]    = {0};
    texts[]     = {" "};
    default     = 0;
};

