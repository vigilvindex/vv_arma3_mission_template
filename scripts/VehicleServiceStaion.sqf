/*
TITLE: Vehicle Repair, Refuel, Rearm Pad
AUTHOR: =7Cav=CW2.Iher.I
VERSION: v1
UPDATED: March 3, 2013
DESCRIPTION:
The vehicle pad script included here is a script that instantly facilitates the ability to rearm, repair, and refuel any vehicle in ArmA III.
Originally, the vehicle pad script was considered a private script used only in 7th Cavalry training servers.
I decided that due to the lack of support trucks in the ArmA III Alpha, the public might benefit from a script that lets them service their vehicles.
This script is an adaptation of the script we use on our training servers.
Eventually I will release an ArmA III adapted version of our aircraft failure scripts for use by the public.
I hope you enjoy this script.
USAGE:
1) REQUIRED - Place a trigger on your map. The defined area of the trigger in the editor translates to the area a vehicle must enter to be serviced.
2) REQUIRED - Ensure that the trigger is set to be activated repeatedly and may be activated by anyone or whomever you wish it to be activated by.
3a) REQUIRED - FOR LAND VEHICLES, PLACE THIS IN THE TRIGGER CONDITION.
                ("LandVehicle" countType thislist  > 0) && ((getpos (thislist select 0)) select 2 < 1)
3b) REQUIRED - FOR AIR VEHICLES, PLACE THIS IN THE TRIGGER CONDITION AND REPLACE TRIGGERNAME WITH THE NAME OF THE TRIGGER.
                (getPos ((list triggername) select 0) select 2) <= 1
	
4) REQUIRED - Place the following into the Trigger On Act field.
	_xhandle= (thislist select 0) execVM "x_reload.sqf";
		- This ensures that the script fires upon trigger activation.
5) REQUIRED - Copy & Paste the included x_reload.sqf into your mission file.
6) REQUIRED - Export your mission to pack it into a PBO. Enjoy!

To change the speed at which the pad performs repair, rearm, and refuel services:
	1) OPEN FILE: x_reload.sqf
	2) FIND LINE OF CODE (x_reload_time_factor = 0.01;):  This controls how fast the script runs
	3) CHANGE TO DESIRED VALUE.
CHANGELOG:
1.1     - New conditions to increase reliability, especially with air vehicles. Included sample mission and changed readme.
1.0	- Initial release
*/
_object = _this;
_type = typeOf _object;
x_reload_time_factor = 0.01;
_object setVehicleAmmo 1;
_object vehicleChat format ["Servicing %1... Please stand by...", _type];
_magazines = getArray(configFile >> "CfgVehicles" >> _type >> "magazines");
if (count _magazines > 0) then {
	_removed = [];
	{
		if (!(_x in _removed)) then {
			_object removeMagazines _x;
			_removed = _removed + [_x];
		};
	} forEach _magazines;
	{
		_object vehicleChat format ["Reloading %1", _x];
		sleep x_reload_time_factor;
		_object addMagazine _x;
	} forEach _magazines;
};
_count = count (configFile >> "CfgVehicles" >> _type >> "Turrets");
if (_count > 0) then {
	for "_i" from 0 to (_count - 1) do {
		scopeName "xx_reload2_xx";
		_config = (configFile >> "CfgVehicles" >> _type >> "Turrets") select _i;
		_magazines = getArray(_config >> "magazines");
		_removed = [];
		{
			if (!(_x in _removed)) then {
				_object removeMagazines _x;
				_removed = _removed + [_x];
			};
		} forEach _magazines;
		{
			_object vehicleChat format ["Reloading %1", _x];
			sleep x_reload_time_factor;
			_object addMagazine _x;
			sleep x_reload_time_factor;
		} forEach _magazines;
		_count_other = count (_config >> "Turrets");
		if (_count_other > 0) then {
			for "_i" from 0 to (_count_other - 1) do {
				_config2 = (_config >> "Turrets") select _i;
				_magazines = getArray(_config2 >> "magazines");
				_removed = [];
				{
					if (!(_x in _removed)) then {
						_object removeMagazines _x;
						_removed = _removed + [_x];
					};
				} forEach _magazines;
				{
					_object vehicleChat format ["Reloading %1", _x]; 
					sleep x_reload_time_factor;
					_object addMagazine _x;
					sleep x_reload_time_factor;
				} forEach _magazines;
			};
		};
	};
};
_object setVehicleAmmo 1;	// Reload turrets / drivers magazine
sleep x_reload_time_factor;
_object vehicleChat "Repairing...";
_object setDamage 0;
sleep x_reload_time_factor;
_object vehicleChat "Refueling...";
while {fuel _object < 0.99} do {
	//_object setFuel ((fuel _vehicle + 0.1) min 1);
	_object setFuel 1;
	sleep 0.01;
};
sleep x_reload_time_factor;
_object vehicleChat format ["%1 is ready...", _type];
if (true) exitWith {};