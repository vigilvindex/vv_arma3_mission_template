// Random House Garrison Script
// Ver 1.2 By Desrat
// CBA NO LONGER REQUIRED
// Thanks to BIS, CBA Team & F2K_Sel
// All make this script possible

/////////////////////////////////////
// Example:
//
// _h = ["marker_name"(string), Max Distance from center to spawn(number), side (1-east, 2-west), Debug Markers(true - optional)] execVM "RndmHse.sqf;
//
// _h = ["Target_Area", 100, 1, true] execVM "RndmHse.sqf";
///////////////////////////////////// 

private ["_rtn","_ary","_bldPositions","_i","_bldInfo","_house","_noPos","_skip","_mkrName","_marker","_grp","_unit","_spawnUnit","_targetIndex","_spawnPosArray","_spawnPos","_shuffled","_centre","_dist","_side","_debug","_unitArrayE","_unitArrayW","_blds","_cnt","_posUsed"];

_centre = getMarkerPos (_this select 0);
_dist = _this select 1;
_side = _this select 2;
// optional debug markers
_debug = if(count _this > 3) then {_rtn = _this select 3;_rtn} else {false;};

_unitArrayE = ["O_soldier_repair_F","O_soldier_GL_F","O_Soldier_F","O_soldier_GL_F","O_soldier_AR_F","O_Soldier_F","O_Soldier_LAT_F","O_Medic_F","O_soldier_GL_F","O_Soldier_AR_F","O_soldier_exp_F","O_soldier_F","O_Soldier_SL_F","O_soldier_TL_F","O_soldier_LAT_F"];

_unitArrayW = ["B_soldier_AR_F","B_soldier_exp_F","B_Soldier_GL_F","B_Helipilot_F","B_soldier_M_F","B_medic_F","B_Soldier_F","B_soldier_repair_F","B_soldier_LAT_F","B_Soldier_SL_F","B_Soldier_lite_F","B_Soldier_TL_F"];
/////////////////////////////////////
///// END OF EDITABLE AREA //////////
/////////////////////////////////////
waituntil {!(isnil "bis_fnc_init")}; 

// get all houses within distance
_blds = nearestobjects [[(_centre select 0),(_centre select 1), 0],["house"], _dist];

//declare arrays
_ary = [];
_bldPositions = [];

// populate _ary array with all building positions
{_ary = _ary + [getpos _x]} foreach _blds;

// populate _bldInfo with all possible positions within buildings
{

// cba fnc replaced
//_bldInfo = [(_x select 0),(_x select 1),(_x select 2)] call CBA_fnc_getNearestBuilding;
// cba fnc replaced

_building = nearestBuilding [(_x select 0),(_x select 1),(_x select 2)];
_i = 0;

while {str(_building buildingPos _i) != str([0,0,0])} do {
	_i = _i + 1;
};
_bldInfo = [_building, _i];

_house = _bldInfo select 0;
_noPos = _bldInfo select 1;

	_i = 0;
	while {_i < _noPos} do {
	_bldPositions = _bldPositions + [(_house buildingPos _i)];
	_i = _i + 1;
	};
	
} foreach _ary;

// cba fnc replaced
//_shuffled = _bldPositions call CBA_fnc_shuffle;
// cba fnc replaced

// shuffle array to attempt to add more randomness in spread
_shuffled = []; 
_howManyToSelect = (count _bldPositions); 
while {_howManyToSelect != 0} do { 
   _add = _bldPositions select (floor(random(count _bldPositions))); 
   if (!(_add in _shuffled)) then { 
      _shuffled = _shuffled + [_add]; 
      _howManyToSelect  = _howManyToSelect  - 1; 
   }; 
};

//count array of positions
_cnt = count _shuffled;

// get number of positions to use 0.1 = 10% of all positions available
_posUsed = floor(_cnt * 0.1);

// pick positions randomly and spawn units deleting from array 
// at each iteration to ensure no repeats
_i = 0;
while {(_posUsed > _i) && ((count _shuffled) > 0)} do {
// pick random index from array
_targetIndex = floor(random(count _shuffled));

// position to spawn unit
_spawnPosArray = _shuffled select _targetIndex;
_spawnPos = [(_spawnPosArray select 0),(_spawnPosArray select 1),(_spawnPosArray select 2)];

// returns true/false if another unit within 2m
if (_side == 1) then {
_skip = {side _x == east} count nearestObjects [_spawnPos,["Man","Car","Tank"],10] > 0;
} else {
_skip = {side _x == west} count nearestObjects [_spawnPos,["Man","Car","Tank"],10] > 0;
};

// remove position from array to prevent repeat
_shuffled set [_targetindex,-1];
_shuffled = _shuffled - [-1];

// skip if distance to nearest unit less than 2m
if (!_skip) then {

// create markers if debug enabled
if (_debug) then {
_mkrName = format ["mrk%1",round(random 100000)];

// cba fnc replaced
//_marker = [_mkrName, _spawnPos, "Rectangle", [1, 1],"GLOBAL"] call CBA_fnc_createMarker;
// cba fnc replaced

_marker = createMarker [_mkrName, _spawnPos];
_marker setMarkerShape "RECTANGLE";
_marker setMarkerSize [1, 1];
};

// create unit
if (_side == 1) then {
_grp = creategroup east;
_unit = _unitArrayE select (floor(random(count _unitArrayE)));
} else {
_grp = creategroup west;
_unit = _unitArrayW select (floor(random(count _unitArrayW)));
};

_spawnUnit = _grp createUnit [_unit, [0,0,0], [], 0, "FORM"];
_spawnUnit setPos _spawnPos;
_spawnUnit setDir (floor(random 360));

// patrol building?
_search = [1,0,1,0,1,0,0,0,0,1,0,1,0,0,0,0,0,0] call BIS_fnc_selectRandom;
if (_search == 1) then {
	if (_debug) then {
	// set to ellipse for search
	_marker setMarkerShape "ELLIPSE";
	};

// spawn patrol script
[_spawnUnit,_grp] spawn {
_unit = _this select 0;
_grp = _this select 1;
_building = nearestBuilding (getPos _unit);
_i = 0;

while {str(_building buildingPos _i) != str([0,0,0])} do {
	_i = _i + 1;
};
_bldInfo = [_building, _i];

_house = _bldInfo select 0;
_noPos = _bldInfo select 1;	
	
	while {alive _unit} do {
	_unit setBehaviour "SAFE";
	_grp setSpeedMode "LIMITED";
	_newPosNumber = floor(random _noPos);
	_unit doMove (_house buildingPos _newPosNumber);
	sleep (floor((random 120) + 60));
	};
};


};
// increase index for while loop
_i = _i + 1;
};
};