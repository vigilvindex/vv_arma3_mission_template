/* getParams.sqf
 * Last Updated: 2013/02/15
 * Description: Creates local variables for all values in paramsArray.
 * Arguments: None.
 * TO DO:
 *   Add check for existing variables.
 *   Add check for created variable scopes.
 */
for [{_i = 0},{_i < count(paramsArray)},{_i = _i + 1}] do {
  call compile format ["%1 = %2",(configName ((missionConfigFile >> "Params") select _i)),(paramsArray select _i)];
};
