killTicker = {
	if (isServer) then {
		_this addEventHandler ['Killed',{
			_unit = _this select 0;
			_killer = _this select 1;
			newKill = [_unit,_killer];
			publicVariable "newKill";
			if (!isDedicated) then {
				//for hosting player
				_kill = newKill call parseKill;
				killArray set [count killArray,_kill];
				[] spawn killList;
				if (player == _killer) then {newKill spawn killText};
			};
		}];
	};
};
parseKill = {
	_line = "";
	_killerName = "";
	_victimName = "";
	_killerString = "";
	_victimString = "";
	_killerColor = "#99D5FF";
	_victimColor = "#99D5FF";
	_victim = _this select 0;
	_killer = _this select 1;
	if (!(isplayer _killer)) then {
		_killerName = getText (configFile >> "CfgVehicles" >> format["%1",typeOf _killer] >> "Displayname");
		if(vehicle _killer != _killer) then {
            _killerName = getText (configFile >> "CfgVehicles" >> format["%1 crew",typeof vehicle _killer] >> "Displayname");
        };
    }else{
        _killerName = name _killer;
    };
	if (!(isplayer _victim)) then {
		_victimName = getText (configFile >> "CfgVehicles" >> format["%1",typeOf _victim] >> "Displayname");
		if(vehicle _victim != _victim) then {
            _victimName = getText (configFile >> "CfgVehicles" >> format["%1 crew",typeof vehicle _victim] >> "Displayname");
        };
	}else{
        _victimName = name _victim;
    };
	if ((_killer == player) or (_killer == vehicle player)) then {
		_killerColor = "#ffff00"; //yellow
	}else{
		_killerColor = (side group _killer call BIS_fnc_sideColor) call BIS_fnc_colorRGBtoHTML;
	};
	if (_victim == player) then {
		_victimColor = "#ffff00"; //yellow
	}else{
		_victimColor = (side group _victim call BIS_fnc_sideColor) call BIS_fnc_colorRGBtoHTML;
	};
	_killerString = format["<t color='%1'>%2</t>",_killerColor ,_killerName];
	_victimString = format["<t color='%1'>%2</t>",_victimColor,_victimName];
	_line = switch(true) do {
		case(_killer == _victim): {format ["%1 killed themselves",_killerString]};
		case(isNull _killer): {format ["Bad luck for %1",_victimString]};
		default {format ["%1 killed %2",_killerString,_victimString]};
	};
	_line;
};
killText = {
	_victim = _this select 0;
	_killer = _this select 1;
	_victimName = "";	
	_victimString = "";
	_victimColor = "#99D5FF";
	if (!(isplayer _victim)) then {
		_victimName = getText (configFile >> "CfgVehicles" >> format["%1",typeOf _victim] >> "Displayname");
        if(vehicle _victim != _victim) then {
            _victimName = getText (configFile >> "CfgVehicles" >> format["%1 crew",typeof vehicle _victim] >> "Displayname");
        };
	}else{
        _victimName = name _victim;
    };
	_victimColor = (side group _victim call BIS_fnc_sideColor) call BIS_fnc_colorRGBtoHTML;
	_victimString = format["<t color='%1'>%2</t>",_victimColor,_victimName];
	if ((_killer == player) and (_victim == player)) then {
        [format ["<t size='0.5'>You killed yourself</t>"],0,0.8,2,0,0,8] spawn bis_fnc_dynamicText;
    }else{
		[format ["<t size='0.5'>You killed %1</t>",_victimString],0,0.8,2,0,0,8] spawn bis_fnc_dynamicText;
	};	
};
killList = {
	_tickerLines = [];
	_c = 0;
	for "_i" from (count killArray - 1) to 0 step -1 do {
		_c=_c+1;
		if ( _c == 8 ) exitWith {};
		_kill = killArray select _i;
		_tickerLines set [count _tickerLines,_kill];
	};
	_tickerText = "";
	for "_i" from (count _tickerLines) to 0 step -1 do {
        _tickerText = format ["%1<br />%2",_tickerLines select _i,_tickerText];
	};
	hintsilent parsetext format ["%1",_tickerText];
};
//declare global variables
if (isnil {newKill}) then {newKill = []};
killArray = [];
if (!isDedicated) then {
	//add eventhandler to register new kills from server
	"newKill" addPublicVariableEventhandler {
		_kill = (_this select 1) call parseKill;
		//flush array when it reaches gratutious amount of kills
		if (count killArray > 100) then {killArray = []};
		killArray set [count killArray,_kill];
		[] spawn killList;
		(_this select 1) spawn killText;
	};
};
//make killTicker run on the clients
{ _x spawn killTicker } forEach playableUnits;
//make killTicker run on the ai
{ if (!(isPlayer _x) and !(_x in playableUnits)) then { _x spawn killTicker; }; } forEach allUnits;